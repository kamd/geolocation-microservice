using System;
using System.Threading.Tasks;
using FluentAssertions;
using Kamd.GeolocationService.Services;
using Moq;
using Serilog;
using StackExchange.Redis;
using Xunit;

namespace Kamd.Geolocation.Service.Tests.UnitTests.Services
{
    public class DataCacheTests
    {
        private const string CACHE_KEY = "charizard";
        private const string SERIALISED_TEST_OBJECT = @"{""hitPoints"":5,""favouriteMove"":""Hyper Beam""}";
        
        [Fact]
        public async Task GetItem_ItemExistsInCache_ReturnsItem()
        {
            // arrange
            
            Mock<IDatabase> mockDatabase = new();
            mockDatabase.Setup(d => d.StringGetAsync(CACHE_KEY, CommandFlags.None))
                .ReturnsAsync(SERIALISED_TEST_OBJECT);

            Mock<ILogger> mockLogger = new();

            DataCache dataCache = new(mockDatabase.Object, mockLogger.Object);

            TestObject expectedResponse = GetTestObject();

            // act
            TestObject response = await dataCache.GetItemAsync<TestObject>(CACHE_KEY);

            // assert
            response.Should().BeEquivalentTo(expectedResponse);
        }

        [Fact]
        public async Task GetItem_ItemNotInCache_ReturnsNull()
        {
            // arrange
            Mock<IDatabase> mockDatabase = new();
            mockDatabase.Setup(d => d.StringGetAsync(CACHE_KEY, CommandFlags.None))
                .ReturnsAsync((string)null);

            Mock<ILogger> mockLogger = new();

            DataCache dataCache = new(mockDatabase.Object, mockLogger.Object);

            // act
            TestObject response = await dataCache.GetItemAsync<TestObject>(CACHE_KEY);

            // assert
            response.Should().BeNull();
        }
        
        [Fact]
        public async Task SetItem_SavesItemToCache()
        {
            // arrange
            Mock<IDatabase> mockDatabase = new();
            mockDatabase.Setup(d =>
                    d.StringSetAsync(
                        CACHE_KEY,
                        SERIALISED_TEST_OBJECT,
                        It.IsAny<TimeSpan>(),
                        When.Always,
                        CommandFlags.None))
                .ReturnsAsync(true)
                .Verifiable();

            Mock<ILogger> mockLogger = new();

            DataCache dataCache = new(mockDatabase.Object, mockLogger.Object);

            // act
            await dataCache.SetItemAsync(CACHE_KEY, GetTestObject());

            // assert
            mockDatabase.Verify();
        }
        
        private TestObject GetTestObject() => new()
        {
            HitPoints = 5,
            FavouriteMove = "Hyper Beam"
        };

        public class TestObject
        {
            public int HitPoints { get; set; }
            public string FavouriteMove { get; set; }
        }
    }
}