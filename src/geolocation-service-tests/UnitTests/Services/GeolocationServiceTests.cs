using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using FluentAssertions;
using Kamd.GeolocationService.AutomapperProfiles;
using Kamd.GeolocationService.Exceptions;
using Kamd.GeolocationService.Models;
using Kamd.GeolocationService.Models.Enum;
using Kamd.GeolocationService.Models.Transport.Response;
using Kamd.GeolocationService.Repositories;
using Kamd.GeolocationService.Services;
using Microsoft.AspNetCore.Http;
using Moq;
using Serilog;
using Xunit;

namespace Kamd.Geolocation.Service.Tests.UnitTests.Services
{
    public class GeolocationServiceTests
    {
        private const string TEST_IP_STRING = "1.2.3.4";
        private const string CACHE_KEY = "geolocation:1.2.3.4";

        [Fact]
        public async Task GetLocation_ValidResponseInCache_ReturnsOk()
        {
            // arrange
            Mock<IDataCache> mockCache = new();
            mockCache.Setup(c => c.GetItemAsync<GeolocationService.Models.Entities.Geolocation>("geolocation:1.2.3.4"))
                .ReturnsAsync(GetSuccessGeolocation);
            
            Mock<ILogger> mockLogger = new();
            Mock<IGeolocationRepository> mockRepository = new();
            Mock<IIpApiService> mockApiService = new();
            
            GeolocationService.Services.GeolocationService geolocationService =
                new(mockApiService.Object, GetMapper(), mockRepository.Object, mockCache.Object, mockLogger.Object);

            // act
            ServiceResponse<GeolocationResponse> response = await geolocationService.GetLocation(IPAddress.Parse(TEST_IP_STRING));

            // assert
            response.StatusCode.Should().Be(StatusCodes.Status200OK);
            response.Response.Should().BeEquivalentTo(GetSuccessResponse());
        }
        
        [Fact]
        public async Task GetLocation_ValidResponseInDatabase_ReturnsOkAndSetsCache()
        {
            // arrange
            Mock<IDataCache> mockCache = new();
            mockCache.Setup(c => c.GetItemAsync<GeolocationService.Models.Entities.Geolocation>(CACHE_KEY))
                .ReturnsAsync((GeolocationService.Models.Entities.Geolocation)null);
            mockCache.Setup(c => c.SetItemAsync(CACHE_KEY, It.IsAny<GeolocationService.Models.Entities.Geolocation>()))
                .Verifiable();

            Mock<IGeolocationRepository> mockRepository = new();
            mockRepository.Setup(r => r.GetGeolocation(It.Is<IPAddress>(ip => ip.ToString() == TEST_IP_STRING)))
                .ReturnsAsync(GetSuccessGeolocation);
            
            Mock<ILogger> mockLogger = new();
            Mock<IIpApiService> mockApiService = new();
            
            GeolocationService.Services.GeolocationService geolocationService =
                new(mockApiService.Object, GetMapper(), mockRepository.Object, mockCache.Object, mockLogger.Object);

            // act
            ServiceResponse<GeolocationResponse> response = await geolocationService.GetLocation(IPAddress.Parse(TEST_IP_STRING));

            // assert
            response.StatusCode.Should().Be(StatusCodes.Status200OK);
            response.Response.Should().BeEquivalentTo(GetSuccessResponse());
            mockCache.Verify();
        }
        
        [Fact]
        public async Task GetLocation_ValidResponseFromApi_ReturnsOkAndStoresData()
        {
            // arrange
            Mock<IDataCache> mockCache = new();
            mockCache.Setup(c => c.GetItemAsync<GeolocationService.Models.Entities.Geolocation>(CACHE_KEY))
                .ReturnsAsync((GeolocationService.Models.Entities.Geolocation)null);
            mockCache.Setup(c => c.SetItemAsync(CACHE_KEY, It.IsAny<GeolocationService.Models.Entities.Geolocation>()))
                .Verifiable();

            Mock<IGeolocationRepository> mockRepository = new();
            mockRepository.Setup(r => r.GetGeolocation(It.Is<IPAddress>(ip => ip.ToString() == TEST_IP_STRING)))
                .ReturnsAsync((GeolocationService.Models.Entities.Geolocation)null);
            mockRepository.Setup(r => r.SetGeolocation(It.IsAny<GeolocationService.Models.Entities.Geolocation>()))
                .Verifiable();
            
            Mock<ILogger> mockLogger = new();
            Mock<IIpApiService> mockApiService = new();
            mockApiService.Setup(a => a.GeolocateIpAsync(It.Is<IPAddress>(ip => ip.ToString() == TEST_IP_STRING)))
                .ReturnsAsync(GetSuccessGeolocation);
            
            GeolocationService.Services.GeolocationService geolocationService =
                new(mockApiService.Object, GetMapper(), mockRepository.Object, mockCache.Object, mockLogger.Object);

            // act
            ServiceResponse<GeolocationResponse> response = await geolocationService.GetLocation(IPAddress.Parse(TEST_IP_STRING));

            // assert
            response.StatusCode.Should().Be(StatusCodes.Status200OK);
            response.Response.Should().BeEquivalentTo(GetSuccessResponse());
            mockCache.Verify();
            mockRepository.Verify();
        }

        [Fact]
        public async Task GetLocation_ApiThrowsException_ReturnsServiceUnavailable()
        {
            // arrange
            Mock<IDataCache> mockCache = new();
            mockCache.Setup(c => c.GetItemAsync<GeolocationService.Models.Entities.Geolocation>(CACHE_KEY))
                .ReturnsAsync((GeolocationService.Models.Entities.Geolocation)null);

            Mock<IGeolocationRepository> mockRepository = new();
            mockRepository.Setup(r => r.GetGeolocation(It.Is<IPAddress>(ip => ip.ToString() == TEST_IP_STRING)))
                .ReturnsAsync((GeolocationService.Models.Entities.Geolocation)null);

            Mock<ILogger> mockLogger = new();
            Mock<IIpApiService> mockApiService = new();
            mockApiService.Setup(a => a.GeolocateIpAsync(It.Is<IPAddress>(ip => ip.ToString() == TEST_IP_STRING)))
                .ThrowsAsync(new IpApiException());
            
            GeolocationService.Services.GeolocationService geolocationService =
                new(mockApiService.Object, GetMapper(), mockRepository.Object, mockCache.Object, mockLogger.Object);

            // act
            ServiceResponse<GeolocationResponse> response = await geolocationService.GetLocation(IPAddress.Parse(TEST_IP_STRING));

            // assert
            response.StatusCode.Should().Be(StatusCodes.Status503ServiceUnavailable);
        }
        
        [Fact]
        public async Task GetLocation_ApiReturnsNull_ReturnsServiceUnavailable()
        {
            // arrange
            Mock<IDataCache> mockCache = new();
            mockCache.Setup(c => c.GetItemAsync<GeolocationService.Models.Entities.Geolocation>(CACHE_KEY))
                .ReturnsAsync((GeolocationService.Models.Entities.Geolocation)null);

            Mock<IGeolocationRepository> mockRepository = new();
            mockRepository.Setup(r => r.GetGeolocation(It.Is<IPAddress>(ip => ip.ToString() == TEST_IP_STRING)))
                .ReturnsAsync((GeolocationService.Models.Entities.Geolocation)null);

            Mock<ILogger> mockLogger = new();
            Mock<IIpApiService> mockApiService = new();
            mockApiService.Setup(a => a.GeolocateIpAsync(It.Is<IPAddress>(ip => ip.ToString() == TEST_IP_STRING)))
                .ReturnsAsync((GeolocationService.Models.Entities.Geolocation)null);
            
            GeolocationService.Services.GeolocationService geolocationService =
                new(mockApiService.Object, GetMapper(), mockRepository.Object, mockCache.Object, mockLogger.Object);

            // act
            ServiceResponse<GeolocationResponse> response = await geolocationService.GetLocation(IPAddress.Parse(TEST_IP_STRING));

            // assert
            response.StatusCode.Should().Be(StatusCodes.Status503ServiceUnavailable);
        }
        
        [Theory]
        [InlineData(GeolocationStatus.InvalidQuery)]
        [InlineData(GeolocationStatus.PrivateRange)]
        [InlineData(GeolocationStatus.ReservedRange)]
        public async Task GetLocation_FailedResponseFromApi_ReturnsBadRequestAndStoresData(GeolocationStatus geolocationStatus)
        {
            // arrange
            Mock<IDataCache> mockCache = new();
            mockCache.Setup(c => c.GetItemAsync<GeolocationService.Models.Entities.Geolocation>(CACHE_KEY))
                .ReturnsAsync((GeolocationService.Models.Entities.Geolocation)null);
            mockCache.Setup(c => c.SetItemAsync(CACHE_KEY, It.IsAny<GeolocationService.Models.Entities.Geolocation>()))
                .Verifiable();

            Mock<IGeolocationRepository> mockRepository = new();
            mockRepository.Setup(r => r.GetGeolocation(It.Is<IPAddress>(ip => ip.ToString() == TEST_IP_STRING)))
                .ReturnsAsync((GeolocationService.Models.Entities.Geolocation)null);
            mockRepository.Setup(r => r.SetGeolocation(It.IsAny<GeolocationService.Models.Entities.Geolocation>()))
                .Verifiable();
            
            Mock<ILogger> mockLogger = new();
            Mock<IIpApiService> mockApiService = new();
            mockApiService.Setup(a => a.GeolocateIpAsync(It.Is<IPAddress>(ip => ip.ToString() == TEST_IP_STRING)))
                .ReturnsAsync(GetFailedGeolocation(geolocationStatus));
            
            GeolocationService.Services.GeolocationService geolocationService =
                new(mockApiService.Object, GetMapper(), mockRepository.Object, mockCache.Object, mockLogger.Object);

            // act
            ServiceResponse<GeolocationResponse> response = await geolocationService.GetLocation(IPAddress.Parse(TEST_IP_STRING));

            // assert
            response.StatusCode.Should().Be(StatusCodes.Status400BadRequest);
            mockCache.Verify();
            mockRepository.Verify();
        }

        private IMapper GetMapper()
        {
            var config = new MapperConfiguration(cfg => cfg.AddProfile(typeof(GeolocationProfile)));
            var mapper = config.CreateMapper();
            return mapper;
        }

        private GeolocationResponse GetSuccessResponse() => new()
        {
            IpAddress = TEST_IP_STRING,
            Latitude = 12.56789m,
            Longitude = 5.12321m
        };

        private GeolocationService.Models.Entities.Geolocation GetSuccessGeolocation() => new()
        {
            Id = 12345,
            IpAddress = IPAddress.Parse(TEST_IP_STRING),
            Latitude = 12.56789m,
            Longitude = 5.12321m,
            Status = GeolocationStatus.Success
        };

        private GeolocationService.Models.Entities.Geolocation GetFailedGeolocation(GeolocationStatus status) => new()
        {
            IpAddress = IPAddress.Parse(TEST_IP_STRING),
            Status = status
        };
    }
}