using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentAssertions;
using Kamd.GeolocationService.AutomapperProfiles;
using Kamd.GeolocationService.Exceptions;
using Kamd.GeolocationService.Models.Enum;
using Kamd.GeolocationService.Services;
using Moq;
using Moq.Protected;
using Serilog;
using Xunit;

namespace Kamd.Geolocation.Service.Tests.UnitTests.Services
{
    public class IpApiServiceTests
    {
        private const string RATE_LIMIT_REQUESTS_REMAINNG_HEADER_KEY = "X-Rl";
        private const string RATE_LIMIT_TIME_TO_NEXT_WINDOW_HEADER_KEY = "X-Ttl";
        
        [Fact]
        public async Task GeolocateIpAsync_ApiReturnsGoodResponse_ReturnsCorrectGeolocation()
        {
            // arrange
            const string requestedIp = "1.2.3.4";

            HttpResponseMessage httpResponseMessage = GetSuccessfulApiResponse(requestedIp);

            GeolocationService.Models.Entities.Geolocation expectedResponse = new()
            {
                Status = GeolocationStatus.Success,
                IpAddress = IPAddress.Parse(requestedIp),
                Latitude = 38.4225m,
                Longitude = -82.4492m
            };

            string requestUri = RequestUrlForIpAddress(requestedIp);
            
            Mock<HttpMessageHandler> mockHttpMessageHandler = new(MockBehavior.Strict);
            mockHttpMessageHandler
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.Is<HttpRequestMessage>(r => r.RequestUri.AbsoluteUri == requestUri),
                    ItExpr.IsAny<CancellationToken>()
                )
                .ReturnsAsync(httpResponseMessage)
                .Verifiable();
            
            HttpClient httpClient = new(mockHttpMessageHandler.Object);
            
            Mock<ILogger> mockLogger = new();
            Mock<IIpApiRateLimitFlag> mockRateLimitFlag = new();

            IpApiService ipApiService = new(httpClient, GetMapper(), mockLogger.Object, mockRateLimitFlag.Object);
            
            // act
            GeolocationService.Models.Entities.Geolocation response =
                await ipApiService.GeolocateIpAsync(IPAddress.Parse(requestedIp));

            // assert
            mockHttpMessageHandler.Verify();
            response.Should().BeEquivalentTo(expectedResponse);
        }
        
        [Fact]
        public async Task GeolocateIpAsync_ApiThrowsException_ThrowsIpApiException()
        {
            // arrange
            const string requestedIp = "1.2.3.4";
            string requestUri = RequestUrlForIpAddress(requestedIp);
            
            Mock<HttpMessageHandler> mockHttpMessageHandler = new(MockBehavior.Strict);
            mockHttpMessageHandler
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.Is<HttpRequestMessage>(r => r.RequestUri.AbsoluteUri == requestUri),
                    ItExpr.IsAny<CancellationToken>()
                )
                .ThrowsAsync(new IpApiException())
                .Verifiable();
            
            HttpClient httpClient = new(mockHttpMessageHandler.Object);
            
            Mock<ILogger> mockLogger = new();
            Mock<IIpApiRateLimitFlag> mockRateLimitFlag = new();

            IpApiService ipApiService = new(httpClient, GetMapper(), mockLogger.Object, mockRateLimitFlag.Object);
            
            // act / assert
            await Assert.ThrowsAsync<IpApiException>(() => ipApiService.GeolocateIpAsync(IPAddress.Parse(requestedIp)));

            // assert
            mockHttpMessageHandler.Verify();
        }
        
        [Fact]
        public async Task GeolocateIpAsync_ApiTooManyRequests_ThrowsIpApiExceptionAndSetsRateLimitFlag()
        {
            // arrange
            const string requestedIp = "1.2.3.4";
            string requestUri = RequestUrlForIpAddress(requestedIp);

            HttpResponseMessage responseMessage = new(HttpStatusCode.TooManyRequests)
            {
                Headers =
                {
                    { RATE_LIMIT_REQUESTS_REMAINNG_HEADER_KEY, "0" },
                    { RATE_LIMIT_TIME_TO_NEXT_WINDOW_HEADER_KEY, "10" }
                }
            };
            
            Mock<HttpMessageHandler> mockHttpMessageHandler = new(MockBehavior.Strict);
            mockHttpMessageHandler
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.Is<HttpRequestMessage>(r => r.RequestUri.AbsoluteUri == requestUri),
                    ItExpr.IsAny<CancellationToken>()
                )
                .ReturnsAsync(responseMessage)
                .Verifiable();
            
            HttpClient httpClient = new(mockHttpMessageHandler.Object);
            
            Mock<ILogger> mockLogger = new();
            Mock<IIpApiRateLimitFlag> mockRateLimitFlag = new();
            mockRateLimitFlag.SetupSet(f => f.CurrentlyRateLimited = true)
                .Verifiable();

            IpApiService ipApiService = new(httpClient, GetMapper(), mockLogger.Object, mockRateLimitFlag.Object);
            
            // act / assert
            await Assert.ThrowsAsync<IpApiException>(() => ipApiService.GeolocateIpAsync(IPAddress.Parse(requestedIp)));

            // assert
            mockHttpMessageHandler.Verify();
            mockRateLimitFlag.Verify();
        }
        
        
        [Fact]
        public async Task GeolocateIpAsync_ApiIndicatesNoMoreRequestsAllowed_SetsRateLimitFlag()
        {
            // arrange
            const string requestedIp = "1.2.3.4";
            string requestUri = RequestUrlForIpAddress(requestedIp);
            
            HttpResponseMessage rateLimitingResponseMessage =
                GetSuccessfulApiResponse(requestedIp, rateLimitHeaders: true);
            
            Mock<HttpMessageHandler> mockHttpMessageHandler = new(MockBehavior.Strict);
            mockHttpMessageHandler
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.Is<HttpRequestMessage>(r => r.RequestUri.AbsoluteUri == requestUri),
                    ItExpr.IsAny<CancellationToken>()
                )
                .ReturnsAsync(rateLimitingResponseMessage);

            HttpClient httpClient = new(mockHttpMessageHandler.Object);
            
            Mock<ILogger> mockLogger = new();
            Mock<IIpApiRateLimitFlag> mockRateLimitFlag = new();
            mockRateLimitFlag.SetupSet(f => f.CurrentlyRateLimited = true)
                .Verifiable();

            IpApiService ipApiService = new(httpClient, GetMapper(), mockLogger.Object, mockRateLimitFlag.Object);
            
            // act
            GeolocationService.Models.Entities.Geolocation goodResponse =
                await ipApiService.GeolocateIpAsync(IPAddress.Parse(requestedIp));

            // assert
            goodResponse.Should().NotBeNull();
            mockRateLimitFlag.Verify();
        }
        
        [Fact]
        public async Task GeolocateIpAsync_ApiIndicatesNoMoreRequestsAllowed_ClearsRateLimitFlagAtNextWindow()
        {
            // arrange
            const string requestedIp = "1.2.3.4";
            string requestUri = RequestUrlForIpAddress(requestedIp);
            
            HttpResponseMessage rateLimitingResponseMessage =
                GetSuccessfulApiResponse(requestedIp, rateLimitHeaders: true);
            
            Mock<HttpMessageHandler> mockHttpMessageHandler = new(MockBehavior.Strict);
            mockHttpMessageHandler
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.Is<HttpRequestMessage>(r => r.RequestUri.AbsoluteUri == requestUri),
                    ItExpr.IsAny<CancellationToken>()
                )
                .ReturnsAsync(rateLimitingResponseMessage);

            HttpClient httpClient = new(mockHttpMessageHandler.Object);
            
            Mock<ILogger> mockLogger = new();
            Mock<IIpApiRateLimitFlag> mockRateLimitFlag = new();

            IpApiService ipApiService = new(httpClient, GetMapper(), mockLogger.Object, mockRateLimitFlag.Object);
            
            // act
            await ipApiService.GeolocateIpAsync(IPAddress.Parse(requestedIp));

            // assert
            mockRateLimitFlag.VerifySet(f => f.CurrentlyRateLimited = true);
            await Task.Delay(1100);
            mockRateLimitFlag.VerifySet(f => f.CurrentlyRateLimited = false);
        }

        [Fact]
        public async Task GeolocateIpAsync_NullIpAddress_ThrowsArgumentNullException()
        {
            // arrange
            Mock<HttpMessageHandler> mockHttpMessageHandler = new(MockBehavior.Strict);
            mockHttpMessageHandler
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>()
                )
                .ReturnsAsync(new HttpResponseMessage());

            HttpClient httpClient = new(mockHttpMessageHandler.Object);
            
            Mock<ILogger> mockLogger = new();
            Mock<IIpApiRateLimitFlag> mockRateLimitFlag = new();

            IpApiService ipApiService = new(httpClient, GetMapper(), mockLogger.Object, mockRateLimitFlag.Object);
            
            // act / assert
            await Assert.ThrowsAsync<ArgumentNullException>(() => ipApiService.GeolocateIpAsync(null));
        }
        
        [Fact]
        public async Task GeolocateIpAsync_RateLimitFlagActive_ThrowsIpApiException()
        {
            // arrange
            const string requestedIp = "1.2.3.4";
            Mock<HttpMessageHandler> mockHttpMessageHandler = new(MockBehavior.Strict);
            mockHttpMessageHandler
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>()
                )
                .ReturnsAsync(new HttpResponseMessage());

            HttpClient httpClient = new(mockHttpMessageHandler.Object);
            
            Mock<ILogger> mockLogger = new();
            Mock<IIpApiRateLimitFlag> mockRateLimitFlag = new();
            mockRateLimitFlag.SetupGet(f => f.CurrentlyRateLimited)
                .Returns(true)
                .Verifiable();

            IpApiService ipApiService = new(httpClient, GetMapper(), mockLogger.Object, mockRateLimitFlag.Object);
            
            // act / assert
            await Assert.ThrowsAsync<IpApiException>(() => ipApiService.GeolocateIpAsync(IPAddress.Parse(requestedIp)));
            
            // assert
            mockRateLimitFlag.Verify();
        }

        
        private IMapper GetMapper()
        {
            var config = new MapperConfiguration(cfg => cfg.AddProfile(typeof(GeolocationProfile)));
            var mapper = config.CreateMapper();
            return mapper;
        }

        private string RequestUrlForIpAddress(string requestedIp)
        {
            string requestUrl = $"http://ip-api.com/json/{requestedIp}?fields=status,message,lat,lon,query";
            return requestUrl;
        }

        private HttpResponseMessage GetSuccessfulApiResponse(string requestedIp, bool rateLimitHeaders = false)
        {
            HttpResponseMessage httpResponseMessage = new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent($@"{{
                ""status"": ""success"",
                ""lat"": 38.4225,
                ""lon"": -82.4492,
                ""query"": ""{requestedIp}""
            }}"),
                Headers =
                {
                    { RATE_LIMIT_REQUESTS_REMAINNG_HEADER_KEY, rateLimitHeaders ? "0" : "30" },
                    { RATE_LIMIT_TIME_TO_NEXT_WINDOW_HEADER_KEY, "1" }
                }
            };

            return httpResponseMessage;
        }
    }
}