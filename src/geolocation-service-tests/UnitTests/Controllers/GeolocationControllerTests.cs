using System.Net;
using System.Threading.Tasks;
using FluentAssertions;
using Kamd.GeolocationService.Controllers;
using Kamd.GeolocationService.Models;
using Kamd.GeolocationService.Models.Transport.Response;
using Kamd.GeolocationService.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Serilog;
using Xunit;

namespace Kamd.Geolocation.Service.Tests.UnitTests.Controllers
{
    public class GeolocationControllerTests
    {
        [Fact]
        public async Task GetLocation_ValidRequestServiceReturnsOk_ReturnsOk()
        {
            // arrange
            const string requestIp = "1.2.3.4";
            GeolocationResponse expectedResponseContent = GetSuccessResponse(requestIp);
            
            Mock<IGeolocationService> mockService = new();
            mockService.Setup(s => s.GetLocation(It.Is<IPAddress>(ip => Equals(ip, IPAddress.Parse(requestIp)))))
                .ReturnsAsync(new ServiceResponse<GeolocationResponse>()
                {
                    StatusCode = StatusCodes.Status200OK,
                    Response = expectedResponseContent
                });
            
            Mock<ILogger> mockLogger = new();
            GeolocationController controller = new(mockService.Object, mockLogger.Object);
            
            // act
            IActionResult response = await controller.GetLocation(requestIp);

            // assert
            response.Should().BeOfType<OkObjectResult>();
            response.As<OkObjectResult>().Value.Should().Be(expectedResponseContent);
        }
        
        [Fact]
        public async Task GetLocation_ValidRequestServiceReturnsBadRequest_ReturnsBadRequest()
        {
            // arrange
            const string requestIp = "1.2.3.4";
            
            Mock<IGeolocationService> mockService = new();
            mockService.Setup(s => s.GetLocation(It.Is<IPAddress>(ip => Equals(ip, IPAddress.Parse(requestIp)))))
                .ReturnsAsync(new ServiceResponse<GeolocationResponse>()
                {
                    StatusCode = StatusCodes.Status400BadRequest
                });
            
            Mock<ILogger> mockLogger = new();
            GeolocationController controller = new(mockService.Object, mockLogger.Object);
            
            // act
            IActionResult response = await controller.GetLocation(requestIp);

            // assert
            response.Should().BeOfType<ObjectResult>();
            response.As<ObjectResult>().StatusCode.Should().Be(StatusCodes.Status400BadRequest);
            response.As<ObjectResult>().Value.Should().BeOfType<ProblemDetails>();
        }
        
        
        [Fact]
        public async Task GetLocation_ValidRequestServiceReturnsServiceUnavailable_ReturnsServiceUnavailable()
        {
            // arrange
            const string requestIp = "1.2.3.4";
            
            Mock<IGeolocationService> mockService = new();
            mockService.Setup(s => s.GetLocation(It.Is<IPAddress>(ip => Equals(ip, IPAddress.Parse(requestIp)))))
                .ReturnsAsync(new ServiceResponse<GeolocationResponse>()
                {
                    StatusCode = StatusCodes.Status503ServiceUnavailable
                });
            
            Mock<ILogger> mockLogger = new();
            GeolocationController controller = new(mockService.Object, mockLogger.Object);
            
            // act
            IActionResult response = await controller.GetLocation(requestIp);

            // assert
            response.Should().BeOfType<StatusCodeResult>();
            response.As<StatusCodeResult>().StatusCode.Should().Be(StatusCodes.Status503ServiceUnavailable);
        }
        
        
        [Fact]
        public async Task GetLocation_InvalidRequest_ReturnsBadRequest()
        {
            // arrange
            const string requestIp = "abcdef";
            
            Mock<IGeolocationService> mockService = new();
            Mock<ILogger> mockLogger = new();
            GeolocationController controller = new(mockService.Object, mockLogger.Object);
            
            // act
            IActionResult response = await controller.GetLocation(requestIp);

            // assert
            // ASP.NET magically turns this object result into a bad request result later in the pipeline
            response.Should().BeOfType<ObjectResult>(); 
            response.As<ObjectResult>().Value.Should().BeOfType<ValidationProblemDetails>();
            mockService.Verify(s => s.GetLocation(It.IsAny<IPAddress>()), Times.Never);
        }
        
        private GeolocationResponse GetSuccessResponse(string requestIp) => new()
        {
            IpAddress = requestIp,
            Latitude = 12.56789m,
            Longitude = 5.12321m
        };
    }
}