using System;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using FluentAssertions;
using Kamd.GeolocationService.Models.Transport.Response;
using Microsoft.Extensions.Configuration;
using Xunit;

namespace Kamd.Geolocation.Service.Tests.IntegrationTests
{
    public class GeolocationIntegrationTests
    {
        private const string GEOLOCATION_ENDPOINT = "/api/geolocation/";
        private readonly string _serviceUrl;

        private readonly JsonSerializerOptions _serializerOptions = new()
        {
            PropertyNamingPolicy = JsonNamingPolicy.CamelCase
        };
        
        public GeolocationIntegrationTests()
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory)
                .AddJsonFile("appsettings.json")
                .Build();

            string baseUrl = config["GeolocationServiceHost"];
            _serviceUrl = baseUrl + GEOLOCATION_ENDPOINT;
        }

        [Fact]
        public async Task GivenValidRequest_ServiceReturnsOkCorrectResponse()
        {
            // arrange
            const string requestIp = "12.34.56.78";
            
            HttpClient httpClient = GetGeolocationHttpClient();
            GeolocationResponse expectedResponse = new GeolocationResponse()
            {
                IpAddress = requestIp,
                Latitude = 38.4225m,
                Longitude = -82.4492m
            };

            // act
            HttpResponseMessage httpResponse = await httpClient.GetAsync(requestIp);
            string responseJson = await httpResponse.Content.ReadAsStringAsync();
            GeolocationResponse response =
                JsonSerializer.Deserialize<GeolocationResponse>(responseJson, _serializerOptions);

            // assert
            httpResponse.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Should().BeEquivalentTo(expectedResponse);
        }
        
        [Fact]
        public async Task GivenInvalidRequest_ServiceReturnsBadRequest()
        {
            // arrange
            const string requestIp = "abcdef";
            
            HttpClient httpClient = GetGeolocationHttpClient();

            // act
            HttpResponseMessage httpResponse = await httpClient.GetAsync(requestIp);

            // assert
            httpResponse.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }
        
        [Fact]
        public async Task GivenPrivateRangeIpAddress_ServiceReturnsBadRequest()
        {
            // arrange
            const string requestIp = "127.0.0.1";
            
            HttpClient httpClient = GetGeolocationHttpClient();

            // act
            HttpResponseMessage httpResponse = await httpClient.GetAsync(requestIp);

            // assert
            httpResponse.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        private HttpClient GetGeolocationHttpClient()
        {
            HttpClient httpClient = new HttpClient()
            {
                BaseAddress = new Uri(_serviceUrl)
            };
            
            return httpClient;
        }
    }
}