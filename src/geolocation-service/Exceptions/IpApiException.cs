using System;
using System.Runtime.Serialization;

namespace Kamd.GeolocationService.Exceptions
{
    public class IpApiException : Exception
    {
        public IpApiException()
        {
        }

        protected IpApiException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public IpApiException(string message) : base(message)
        {
        }

        public IpApiException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}