using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Reflection;
using Kamd.GeolocationService.Repositories;
using Kamd.GeolocationService.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Npgsql;
using Serilog;
using StackExchange.Redis;
using ILogger = Serilog.ILogger;

namespace Kamd.GeolocationService
{
    [ExcludeFromCodeCoverage]
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            Log.Information("Configuring Services...");
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "geolocation-service", Version = "v1" });
                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            var redisConnectionMultiplexer = ConnectionMultiplexer.Connect(Configuration.GetConnectionString("redis"));
            services.AddScoped<IDatabase>(sp => redisConnectionMultiplexer.GetDatabase());

            services.AddHttpClient<IIpApiService, IpApiService>();

            services.AddScoped<IGeolocationService, Services.GeolocationService>();
            services.AddScoped<IGeolocationRepository, GeolocationRepository>();
            services.AddScoped<IDataCache, DataCache>();

            services.AddDbContext<DataContext>(options => options.UseNpgsql(
                Configuration.GetSection("Database")
                    .Get<NpgsqlConnectionStringBuilder>()
                    .ToString()));

            services.AddSingleton<ILogger>(_ => Log.Logger);
            services.AddSingleton<IIpApiRateLimitFlag, IpApiRateLimitFlag>();
            
            Log.Information("Configuring Services complete");
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            Log.Information("Configuring app...");
            // Probably would do this schema migration in a devops pipeline in the real world
            var dataContext = app.ApplicationServices.GetRequiredService<DataContext>();
            dataContext.Database.Migrate();
            Log.Information("Database migrations ensured");
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            app.UseSerilogRequestLogging();

            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "geolocation-service v1"));

            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            
            Log.Information("Configuring app complete");
        }
    }
}
