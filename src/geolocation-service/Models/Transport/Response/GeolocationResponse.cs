using System.Net;

namespace Kamd.GeolocationService.Models.Transport.Response
{
    public class GeolocationResponse
    {
        public string IpAddress { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
    }
}