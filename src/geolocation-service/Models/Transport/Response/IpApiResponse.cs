namespace Kamd.GeolocationService.Models.Transport.Response
{
    public class IpApiResponse
    {
        public string Status { get; set; }
        public string Message { get; set; }
        public decimal Lat { get; set; }
        public decimal Lon { get; set; }
        public string Query { get; set; }
    }
}