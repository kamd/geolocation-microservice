using System.Net;
using System.Text.Json.Serialization;
using Kamd.GeolocationService.Models.Enum;
using Kamd.GeolocationService.Services;

namespace Kamd.GeolocationService.Models.Entities
{
    public class Geolocation
    {
        public long Id { get; set; }
        
        [JsonConverter(typeof(IpAddressConverter))]
        public IPAddress IpAddress { get; set; }
        
        public GeolocationStatus Status { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
    }
}