namespace Kamd.GeolocationService.Models.Enum
{
    public enum GeolocationStatus
    {
        Success,
        PrivateRange,
        ReservedRange,
        InvalidQuery
    }
}