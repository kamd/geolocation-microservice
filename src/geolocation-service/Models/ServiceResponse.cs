using Microsoft.AspNetCore.Http;

namespace Kamd.GeolocationService.Models
{
    public class ServiceResponse<T>
    {
        public int StatusCode { get; set; }
        public string ErrorMessage { get; set; }
        public T Response { get; set; }
    }
}