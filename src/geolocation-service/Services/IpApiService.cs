using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using AutoMapper;
using Kamd.GeolocationService.Exceptions;
using Kamd.GeolocationService.Models.Entities;
using Kamd.GeolocationService.Models.Transport.Response;
using Microsoft.AspNetCore.Http;
using Serilog;

namespace Kamd.GeolocationService.Services
{
    public interface IIpApiService
    {
        Task<Geolocation> GeolocateIpAsync(IPAddress ipAddress);
    }
    
    public class IpApiService : IIpApiService
    {
        private const string GEOLOCATION_ENDPOINT_URL = @"http://ip-api.com/json/"; // HTTP because HTTPS is paid only!
        // Used to restrict fields returned to the ones we care about
        private const string GEOLOCATION_FIELDS = "fields=status,message,lat,lon,query";
        
        private static readonly JsonSerializerOptions JsonSerializerOptions = new()
        {
            PropertyNamingPolicy = JsonNamingPolicy.CamelCase
        };

        private readonly IIpApiRateLimitFlag _rateLimitFlag;
        private readonly HttpClient _httpClient;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;
        
        public IpApiService(HttpClient httpClient, IMapper mapper, ILogger logger, IIpApiRateLimitFlag rateLimitFlag)
        {
            _httpClient = httpClient;
            _mapper = mapper;
            _logger = logger;
            _rateLimitFlag = rateLimitFlag;
            httpClient.BaseAddress = new Uri(GEOLOCATION_ENDPOINT_URL);
        }

        public async Task<Geolocation> GeolocateIpAsync(IPAddress ipAddress)
        {
            if (ipAddress == null)
            {
                _logger.Error("{Method} called with null IP address", nameof(GeolocateIpAsync));
                throw new ArgumentNullException(nameof(ipAddress));
            }

            if (_rateLimitFlag.CurrentlyRateLimited)
            {
                HandleRateLimitError();
            }

            string fullUrl = $"{GEOLOCATION_ENDPOINT_URL}{ipAddress}?{GEOLOCATION_FIELDS}";
            try
            {
                HttpResponseMessage httpResponseMessage = await _httpClient.GetAsync(fullUrl);

                if (IsRateLimited(httpResponseMessage))
                {
                    HandleRateLimitError();
                }
                
                string response = await httpResponseMessage.Content.ReadAsStringAsync();
                
                _logger.Debug("External API geolocation service called at URL '{Url}', response: '{Response}'",
                    fullUrl,
                    response);

                IpApiResponse ipApiResponse =
                    JsonSerializer.Deserialize<IpApiResponse>(response, JsonSerializerOptions);
                
                Geolocation geolocation = _mapper.Map<Geolocation>(ipApiResponse);
                _logger.Information("Geolocation returned from external API: {@Geolocation}", geolocation);
                
                return geolocation;
            }
            catch (Exception ex)
            {
                const string errorMessage = "Error calling external API";
                _logger.Error(ex, errorMessage);
                throw new IpApiException(errorMessage, ex);
            }
        }

        private void HandleRateLimitError()
        {
            const string errorMessage = "Could not call external API because we are currently being rate-limited";
            _logger.Warning(errorMessage);
            throw new IpApiException(errorMessage);
        }

        private bool IsRateLimited(HttpResponseMessage responseMessage)
        {
            bool requestWasRateLimited = (int)responseMessage.StatusCode == StatusCodes.Status429TooManyRequests;

            // Check rate limiting headers from external API
            if (responseMessage.Headers.TryGetValues("X-Rl", out IEnumerable<string> xR1Values))
            {
                string requestsRemainingHeader = xR1Values.First();
                int requestsRemaining = int.Parse(requestsRemainingHeader);
                _logger.Debug("IP API allowed requests remaining in rate limit window: {Requests}", requestsRemaining);
                
                if (requestsRemaining == 0)
                {
                    string rateLimitWindowTimeToLiveHeader = responseMessage.Headers.GetValues("X-Ttl").First();
                    int rateLimitWindowTimeToLiveSeconds = int.Parse(rateLimitWindowTimeToLiveHeader);
                    
                    _logger.Warning("No more IP API requests allowed for {Seconds} seconds",
                        rateLimitWindowTimeToLiveSeconds);
                    
                    // Reject further calls until the next rate limit window begins
                    _rateLimitFlag.CurrentlyRateLimited = true;
                    Task.Run(async () =>
                    {
                        await Task.Delay(rateLimitWindowTimeToLiveSeconds * 1000);
                        _rateLimitFlag.CurrentlyRateLimited = false;
                        _logger.Information(
                            "Finished waiting for next IP API rate limit window, allowing further requests");
                    });
                }
            }

            return requestWasRateLimited;
        }
    }
}