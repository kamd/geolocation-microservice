using System;
using System.Net;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Kamd.GeolocationService.Services
{
    public class IpAddressConverter : JsonConverter<IPAddress>
    {
        public override IPAddress Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            string ipAddressString = reader.GetString();
            bool successfulParse = IPAddress.TryParse(ipAddressString, out IPAddress ipAddress);
            
            if (!successfulParse)
            {
                throw new InvalidOperationException($"Value is not a valid IP address.");
            }

            return ipAddress;
        }

        public override void Write(Utf8JsonWriter writer, IPAddress value, JsonSerializerOptions options)
        {
            writer.WriteStringValue(value.ToString());
        }
    }
}