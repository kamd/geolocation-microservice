using System.Diagnostics.CodeAnalysis;
using Kamd.GeolocationService.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace Kamd.GeolocationService.Services
{
    [ExcludeFromCodeCoverage]
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        public DbSet<Geolocation> Geolocations { get; set; }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Geolocation>(entity =>
            {
                entity.HasKey(g => g.Id);

                entity.HasIndex(g => g.IpAddress);

                entity.Property(g => g.IpAddress)
                    .IsRequired();
            });
            
            base.OnModelCreating(modelBuilder);
        }
    }
}