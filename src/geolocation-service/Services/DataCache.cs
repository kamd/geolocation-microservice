using System;
using System.Text.Json;
using System.Threading.Tasks;
using Serilog;
using StackExchange.Redis;

namespace Kamd.GeolocationService.Services
{
    public interface IDataCache
    {
        Task<T> GetItemAsync<T>(string key) where T: class;
        Task SetItemAsync<T>(string key, T item);
    }

    public class DataCache : IDataCache
    {
        // In real life this may differ depending on the data
        private static readonly TimeSpan DefaultCacheExpiryTime = new(0, 30, 0);
        
        private static readonly JsonSerializerOptions JsonSerializerOptions = new()
        {
            PropertyNamingPolicy = JsonNamingPolicy.CamelCase
        };
        
        private readonly IDatabase _database;
        private readonly ILogger _logger;
        
        public DataCache(IDatabase database, ILogger logger)
        {
            _database = database;
            _logger = logger;
        }

        public async Task<T> GetItemAsync<T>(string key) where T: class
        {
            string cacheValue = await _database.StringGetAsync(key);

            if (cacheValue == null)
            {
                _logger.Debug("Cache miss, key {Key}", key);
                return null;
            }
            
            T cachedItem = JsonSerializer.Deserialize<T>(cacheValue, JsonSerializerOptions);
            _logger.Debug("Retrieved item from cache, key {Key}, value {@Value}", key, cachedItem);
            return cachedItem;
        }

        public async Task SetItemAsync<T>(string key, T item)
        {
            string stringItem = JsonSerializer.Serialize(item, JsonSerializerOptions);
            await _database.StringSetAsync(key, stringItem, DefaultCacheExpiryTime);
            _logger.Information("Saved item to cache, key {Key}, value {@Value}", key, item);
        }
    }
}