namespace Kamd.GeolocationService.Services
{
    public interface IIpApiRateLimitFlag
    {
        bool CurrentlyRateLimited { get; set; }
    }

    public class IpApiRateLimitFlag : IIpApiRateLimitFlag
    {
        public bool CurrentlyRateLimited { get; set; }
    }
}