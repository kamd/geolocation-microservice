using System;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Kamd.GeolocationService.Exceptions;
using Kamd.GeolocationService.Models;
using Kamd.GeolocationService.Models.Entities;
using Kamd.GeolocationService.Models.Enum;
using Kamd.GeolocationService.Models.Transport.Response;
using Kamd.GeolocationService.Repositories;
using Microsoft.AspNetCore.Http;
using Serilog;

namespace Kamd.GeolocationService.Services
{
    public interface IGeolocationService
    {
        Task<ServiceResponse<GeolocationResponse>> GetLocation(IPAddress ipAddress);
    }

    public class GeolocationService : IGeolocationService
    {
        private const string GEOLOCATION_CACHE_KEY = "geolocation";

        private readonly IIpApiService _ipApiService;
        private readonly IGeolocationRepository _geolocationRepository;
        private readonly IDataCache _dataCache;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        public GeolocationService(
            IIpApiService ipApiService,
            IMapper mapper,
            IGeolocationRepository geolocationRepository,
            IDataCache dataCache,
            ILogger logger)
        {
            _ipApiService = ipApiService;
            _mapper = mapper;
            _geolocationRepository = geolocationRepository;
            _dataCache = dataCache;
            _logger = logger;
        }

        public async Task<ServiceResponse<GeolocationResponse>> GetLocation(IPAddress ipAddress)
        {
            bool updateCache = false;
            bool updateDatabase = false;
            
            // Get result from cache
            string cacheKey = $"{GEOLOCATION_CACHE_KEY}:{ipAddress}";
            Geolocation geolocation = await GetFromCache(ipAddress, cacheKey);

            if (geolocation == null)
            {
                // Get result from database
                updateCache = true;
                geolocation = await GetFromDatabase(ipAddress);    
            }
            
            if (geolocation == null)
            {
                // Get result from external API
                updateDatabase = true;
                try
                {
                    geolocation = await GetFromExternalApi(ipAddress);
                }
                catch (IpApiException ex)
                {
                    _logger.Error(ex, "Error thrown from external API service");
                    
                    var errorResponse = new ServiceResponse<GeolocationResponse>
                    {
                        StatusCode = StatusCodes.Status503ServiceUnavailable
                    };

                    return errorResponse;
                }
            }
            
            if (updateDatabase)
            {
                await _geolocationRepository.SetGeolocation(geolocation);    
                _logger.Debug("Geolocation for IP {IP} saved to database", ipAddress);
            }
            
            if (updateCache)
            {
                await _dataCache.SetItemAsync(cacheKey, geolocation);
                _logger.Debug("Geolocation for IP {IP} saved to cache", ipAddress);
            }

            if (geolocation.Status != GeolocationStatus.Success)
            {
                // Return failed response
                var errorResponse = new ServiceResponse<GeolocationResponse>
                {
                    StatusCode = StatusCodes.Status400BadRequest,
                    ErrorMessage = geolocation.Status switch
                    {
                        GeolocationStatus.InvalidQuery => "IP address is invalid",
                        GeolocationStatus.PrivateRange => "IP address is in private range",
                        GeolocationStatus.ReservedRange => "IP address is in reserved range",
                        _ => throw new ArgumentOutOfRangeException()
                    }
                };

                _logger.Information("Returning failed response for IP {IP}, {@Response}", ipAddress, errorResponse);
                return errorResponse;
            }

            // Return successful response
            var geolocationResponse = _mapper.Map<GeolocationResponse>(geolocation);

            var response = new ServiceResponse<GeolocationResponse>()
            {
                StatusCode = StatusCodes.Status200OK,
                Response = geolocationResponse
            };

            _logger.Information("Returning successful Geolocation response for IP {IP}, {@Response}",
                ipAddress,
                response);
            
            return response;
        }

        private async Task<Geolocation> GetFromCache(IPAddress ipAddress, string cacheKey)
        {
            Geolocation geolocation = await _dataCache.GetItemAsync<Geolocation>(cacheKey);
            
            if (geolocation == null)
            {
                _logger.Debug("Geolocation for IP {IP} was not found in cache", ipAddress);
            }
            else
            {
                _logger.Information("Geolocation for IP {IP} retrieved from cache, '{@Geolocation}'",
                    ipAddress,
                    geolocation);
            }

            return geolocation;
        }
        
        private async Task<Geolocation> GetFromDatabase(IPAddress ipAddress)
        {
            Geolocation geolocation = await _geolocationRepository.GetGeolocation(ipAddress);    
            
            if (geolocation == null)
            {
                _logger.Debug("Geolocation for IP {IP} was not found in database", ipAddress);
            }
            else
            {
                _logger.Information("Geolocation for IP {IP} retrieved from database, '{@Geolocation}'",
                    ipAddress,
                    geolocation);
            }

            return geolocation;
        }

        private async Task<Geolocation> GetFromExternalApi(IPAddress ipAddress)
        {
            Geolocation geolocation = await _ipApiService.GeolocateIpAsync(ipAddress);
            if (geolocation == null)
            {
                const string errorMessage = "External API returned null geolocation";
                _logger.Error(errorMessage);
                throw new IpApiException(errorMessage);
            }

            _logger.Information("Geolocation for IP {IP} retrieved from external API, '{@Geolocation}'",
                ipAddress,
                geolocation);


            return geolocation;
        }
    }
}