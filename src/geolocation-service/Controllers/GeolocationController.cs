using System.Net;
using System.Threading.Tasks;
using Kamd.GeolocationService.Models;
using Kamd.GeolocationService.Models.Transport.Response;
using Kamd.GeolocationService.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace Kamd.GeolocationService.Controllers
{
    [ApiController]
    [Route("api/geolocation")]
    [Produces("application/json")]
    public class GeolocationController : ControllerBase
    {
        private readonly IGeolocationService _geolocationService;
        private readonly ILogger _logger;

        public GeolocationController(IGeolocationService geolocationService, ILogger logger)
        {
            _geolocationService = geolocationService;
            _logger = logger;
        }

        /// <summary>
        /// Gets location from IP address.
        /// </summary>
        /// <param name="ipAddress">Single IP address</param>
        /// <example>12.34.56.78</example>
        /// <returns>Location details associated with the IP address.</returns>
        [HttpGet("{ipAddress}")]
        [ProducesResponseType(typeof(GeolocationResponse), StatusCodes.Status200OK)]
        [ProducesErrorResponseType(typeof(ProblemDetails))]
        public async Task<IActionResult> GetLocation([FromRoute] string ipAddress)
        {
            IPAddress parsedIpAddress = ParseIpAddress(ipAddress);

            if (parsedIpAddress == null)
            {
                ModelState.AddModelError(nameof(ipAddress), "Value was not a valid IP address.");
                return ValidationProblem(ModelState);
            }

            ServiceResponse<GeolocationResponse> response = await _geolocationService.GetLocation(parsedIpAddress);
            _logger.Debug("Response from geolocation service: {@Response}", response);

            IActionResult result = response.StatusCode switch
            {
                StatusCodes.Status200OK => Ok(response.Response),
                StatusCodes.Status400BadRequest => Problem(response.ErrorMessage, statusCode: StatusCodes.Status400BadRequest),
                StatusCodes.Status503ServiceUnavailable => StatusCode(StatusCodes.Status503ServiceUnavailable),
                _ => StatusCode(StatusCodes.Status500InternalServerError)
            };
            
            _logger.Information("Response from GetLocation endpoint: {@Response}", result);

            return result;
        }

        private IPAddress ParseIpAddress(string rawIpAddress)
        {
            bool successfulParse = IPAddress.TryParse(rawIpAddress, out IPAddress ipAddress);
            if (!successfulParse)
            {
                _logger.Warning("Failed to parse requested IP address '{IpAddress}'", rawIpAddress);
                return null;
            }

            _logger.Information("Parsed requested IP address '{IpAddress}' as '{ParsedIpAddress}'", 
                rawIpAddress,
                ipAddress);
            
            return ipAddress;
        }
    }
}