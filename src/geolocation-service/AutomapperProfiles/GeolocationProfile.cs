using System.Net;
using AutoMapper;
using Kamd.GeolocationService.Models.Entities;
using Kamd.GeolocationService.Models.Enum;
using Kamd.GeolocationService.Models.Transport.Response;

namespace Kamd.GeolocationService.AutomapperProfiles
{
    public class GeolocationProfile : Profile
    {
        public GeolocationProfile()
        {
            CreateMap<Geolocation, GeolocationResponse>()
                .ReverseMap();

            CreateMap<IpApiResponse, Geolocation>()
                .ForMember(dest => dest.IpAddress, opt => opt.MapFrom(src => IPAddress.Parse(src.Query)))
                .ForMember(dest => dest.Latitude, opt => opt.MapFrom(src => src.Lat))
                .ForMember(dest => dest.Longitude, opt => opt.MapFrom(src => src.Lon))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => GeolocationStatusFromIpApiResponse(src)))
                .ReverseMap();
        }

        private static GeolocationStatus GeolocationStatusFromIpApiResponse(IpApiResponse ipApiResponse) =>
            ipApiResponse switch
            {
                { Status: "success" } => GeolocationStatus.Success,
                { Status: "fail", Message: "invalid query" } => GeolocationStatus.InvalidQuery,
                { Status: "fail", Message: "private range" } => GeolocationStatus.PrivateRange,
                { Status: "fail", Message: "reserved range" } => GeolocationStatus.ReservedRange,
                _ => GeolocationStatus.InvalidQuery
            };
    }
}