using System.Diagnostics.CodeAnalysis;
using System.Net;
using System.Threading.Tasks;
using Kamd.GeolocationService.Models.Entities;
using Kamd.GeolocationService.Services;
using Microsoft.EntityFrameworkCore;
using Serilog;

namespace Kamd.GeolocationService.Repositories
{
    public interface IGeolocationRepository
    {
        Task<Geolocation> GetGeolocation(IPAddress ipAddress);
        Task SetGeolocation(Geolocation geolocation);
    }

    [ExcludeFromCodeCoverage]
    public class GeolocationRepository : IGeolocationRepository
    {
        private readonly DataContext _context;
        private readonly ILogger _logger;

        public GeolocationRepository(DataContext context, ILogger logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<Geolocation> GetGeolocation(IPAddress ipAddress)
        {
            Geolocation geolocation =
                await _context.Geolocations.FirstOrDefaultAsync(g => Equals(g.IpAddress, ipAddress));

            if (geolocation != null)
            {
                _logger.Debug("Retrieved geolocation from database, ID {ID}, IP {IP}",
                    geolocation.Id,
                    geolocation.IpAddress);
            }
            else
            {
                _logger.Debug("Geolocation not found in database, IP {IP}", ipAddress);
            }

            return geolocation;
        }

        public async Task SetGeolocation(Geolocation geolocation)
        {
            _context.Geolocations.Add(geolocation);
            await _context.SaveChangesAsync();
            
            _logger.Information("Saved geolocation to database, ID {ID}, IP {IP}",
                geolocation.Id,
                geolocation.IpAddress);
        }
    }
}