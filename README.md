
# Geolocation Microservice

## Getting Running

* Ensure docker-compose is installed on your system
* Run `docker-compose up` at the root folder of this repository
* From your host machine you will then be able to hit the following endpoints:
    * Swagger - http://localhost:5007/swagger/index.html
    * Geolocation - GET http://localhost:5007/api/geolocation/{ipAddress}
        * e.g. http://localhost:5007/api/geolocation/1.2.3.4

## Demo Environment

Docker compose pulls up an environment comprising of the `geolocation-service`
microservice, Redis for caching and Postgresql for persistence.

The service calls [IP-API.com](https://ip-api.com/docs/api:json)'s IP Geolocation API
to get its data, and respects the rate limits the API imposes.

## Tests

Unit and integration tests written with xUnit are runnable with `dotnet test`
or through your favourite IDE.

Integration tests require the full system to be running as described above.

## Limitations

* I haven't implemented the full suite of observability, messaging, and
authentication cross-cutting concerns that would be present in a real life
system, but there is structured logging with Serilog
* A document database could be more appropriate than Postgresql given the
  data's unrelated nature, if latency on cache misses was a particular concern,
but I wanted to showcase a more typical Entity-Framework-based persistence
layer
* Geolocation is not an [exact science](https://en.wikipedia.org/wiki/Null_Island) and in
real life I would look for an API that presented more details on the confidence
in its data per request, rather than just one that was callable for free without
any credentials
* The service's API is quite bare bones, just request, latitude and longitude,
  in real life this would be expanded to meet the needs of the consumer (though
this isn't bad for an MVP!)

## Enjoy!
Written by Keir MacDonald.
